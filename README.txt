This modules goal is to support the Hungarian blogring.hu blogring site
with pings. Because this module is useful for Hungarians only, I'll continue
this document in Hungarian only. (If you need it in English, I'll translate
it.)

A blogringhu modul célja a blogring.hu értesítése a „blog” típusú
tartalmak frissítéséről.

Előfeltétel
-----------
- 5.x változatú Drupal
- ping modul engedélyezése

Telepítés
---------
- Regisztrálj a blogring.hu oldalon.
- Másold be a modult a modulok közé (mondjuk
  sites/aries.mindworks.hu/modules).
- Engedélyezd a modult a admin/build/modules oldalon
- Állítsd be a blogring.hu -ra a felhasználói nevet és a jelszót (a jelszó
  az eredeti jelszavad MD5 hash-se!)
- Futtasd a cron.php -t vagy valami hasonlót (például Poor Mans Cron) rendszeresen,
  hogy a telepítés utáni „blog” típusú tartalmakról értesítse a blogring.hu-t.

Készítők
--------
Fehér János (http://aries.mindworks.hu)
A fejlesztést a Mindworks-Networking támogatta (http://mindworks.hu)

Tennivalók
----------
- Többfelhasználóssá tenni.
